<!DOCTYPE html>
<html class=" ">
    <? include('head.php'); ?>

    <!-- BEGIN BODY -->
    <body class=" ">

        <!-- START CONTAINER -->
        <div class="page-container row-fluid">

            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper" style='margin-top:20px;display:inline-block;width:100%;padding:15px 0 0 15px;'>

                    <div class="col-lg-6 col-md-6 col-xs-10 col-sm-10 col-lg-offset-3 col-md-offset-3 col-xs-offset-1 col-xs-offset-1">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title pull-left">Bienvenido a Soy Empleo</h2>                                
                            </header>
                            <div class="content-body">
                                
                                <div class="alert alert-primary alert-dismissible fade in">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            Hola <strong><?=$_REQUEST['nombre']?></strong> gracias por registrarte en soyempleo.com tu email de acceso es: <strong><?=$_REQUEST['email']?></strong></div>

                            <div class='row'>
                                    <div class="pull-right">
                                        <a class="btn btn-success btn-icon" href="index.html">
                                            <i class="fa fa-undo"></i> &nbsp; <span>Volver</span>
                                        </a>
                                    </div>                                    
                                </div>

                            </div>
                            
                        </section></div>




                </section>
            </section>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <? include('js.php'); ?>

    </body>
</html>



